/**
 * Created by ikedi on 02/06/2016.
 */
import React, { Component } from 'react';
import { Platform } from 'react-native';
import Drawer from 'react-native-drawer';
import Main from './main';
import SideMenu from './src/app/core/SideMenu';
import RealmLocalStorage from './src/api/RealmLocalStore';

export default class App extends Component {
    constructor(props){
        super(props);

        //initialize Realm and pass it as a prop
        RealmLocalStorage.init();
        
        this._closeControlPanel = this._closeControlPanel.bind(this);
        this._openControlPanel = this._openControlPanel.bind(this);
        this._renderIcon = this._renderIcon.bind(this);
    }
    _closeControlPanel(){
        this._drawer.close();
    }

    _openControlPanel(){
        this._drawer.open();
    }

    _renderIcon(name) {
        //render a specific Icon based on the platform
        return (Platform.OS === 'ios') ? 'ios-'+name : 'md-'+name;
    }

    render() {
        return (
            <Drawer
                ref={(ref) => this._drawer = ref}
                styles={styles.drawerStyles}
                type="overlay"
                tapToClose={true}
                openDrawerOffset={0.2}
                content={<SideMenu
                            closeMenu={this._closeControlPanel}
                            renderIcon={this._renderIcon}/>
                        }>
                <Main
                    openMenu={this._openControlPanel}
                    renderIcon={this._renderIcon}
                    realmLocalStorage={RealmLocalStorage}
                />
            </Drawer>
        )
    }
}

let styles = {
    drawerStyles: {
        drawer: {
            shadowColor: '#000000',
            shadowOpacity: 0.8,
            shadowRadius: 3
        },
        main: {
            paddingLeft: 3
        }
    }
}