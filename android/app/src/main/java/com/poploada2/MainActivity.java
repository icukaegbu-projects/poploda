package com.poploada2;

import com.facebook.react.ReactActivity;
import io.realm.react.RealmReactPackage;
import com.oblador.vectoricons.VectorIconsPackage;
import com.react.rnspinkit.RNSpinkitPackage;
import com.zfedoran.react.modules.cardscan.*;
import com.facebook.react.ReactPackage;
import com.facebook.react.shell.MainReactPackage;

import java.util.Arrays;
import java.util.List;

public class MainActivity extends ReactActivity {

    /**
     * Returns the name of the main component registered from JavaScript.
     * This is used to schedule rendering of the component.
     */
    @Override
    protected String getMainComponentName() {
        return "poploada2";
    }

    /**
     * Returns whether dev mode should be enabled.
     * This enables e.g. the dev menu.
     */
    @Override
    protected boolean getUseDeveloperSupport() {
        return BuildConfig.DEBUG;
    }

    /**
     * A list of packages used by the app. If the app uses additional views
     * or modules besides the default ones, add more packages here.
     */
    @Override
    protected List<ReactPackage> getPackages() {
        mCardScanPackage = new CardScanPackage(this);

        return Arrays.<ReactPackage>asList(
            mCardScanPackage,
            new MainReactPackage(),
            new RealmReactPackage(),
            new VectorIconsPackage(),
            new RNSpinkitPackage()
        );
    }

    /**
     * Handle card scanning
     */
    private CardScanPackage mCardScanPackage = null;

    @Override
    public void onActivityResult(final int requestCode, final int resultCode, final Intent data) {
          super.onActivityResult(requestCode, resultCode, data);

          mCardScanPackage.handleActivityResult(requestCode, resultCode, data);
    }
}
