/**
 * Created by ikedi on 11/05/2016.
 */
import { Image, View } from 'react-native';
import React, { Component } from 'react';
import Firebase from 'firebase';
import {Router, Scene} from 'react-native-router-flux';

import Login from './src/app/Login';
import Signup from './src/app/Signup';
import ForgotPassword from './src/app/ForgotPassword';
import Profile from './src/app/Profile';
import Home from './src/app/Home';
import Cards from './src/app/cards/Cards';
import Purchase from './src/app/Purchase';
import Transactions from './src/app/Transactions';
import Wallet from './src/app/Wallet';

//let appBaseRef = new Firebase("https://poploada.firebaseio.com")
//let appBaseRef = new Firebase("https://poploada2.firebaseio.com")

export default class Main extends Component {
    constructor(props){
        super(props);

        // this._closeControlPanel = this._closeControlPanel.bind(this);
        // this._openControlPanel = this._openControlPanel.bind(this);
    }
    
    // _closeControlPanel(){
    //     this._drawer.close();
    // }
    //
    // _openControlPanel(){
    //     this._drawer.open();
    // }

    render(){
        //WRAP ALL THE SCENES THAT SHOULD RENDER THE SIDEMENU WITH A SCENE THAT HAS THE
        //DRAWER DEFINED ON IT
        //pass openMenu as props to Router to make it available to all Scenes.
        return (
            <Router
                //appUrl={appBaseRef}
                openMenu={this.props.openMenu}
                renderIcon={this.props.renderIcon}
                realmLocalStorage={this.props.realmLocalStorage}>
                <Scene key="root" hideNavBar={true}>
                    <Scene key="login" component={Login} title="Login" initial={true} />
                    <Scene key="signup" component={Signup} title="Sign Up" />
                    <Scene key="forgotPassword" component={ForgotPassword} title="Sign Up" />

                    <Scene key="home" component={Home} title="Home" />
                    <Scene key="profile" component={Profile} title="Profile"></Scene>
                    <Scene key="wallet" component={Wallet} title="My Wallet"></Scene>
                    <Scene key="purchase" component={Purchase} title="Buy Credit"></Scene>
                    <Scene key="transactions" component={Transactions} title="Transaction History"></Scene>
                    <Scene key="cards" component={Cards} title="Debit Cards"></Scene>
                </Scene>
            </Router>
        );
    }
}

let styles = {
    footer: {
        backgroundColor: '#234FFE'
    },
    badge: {
        backgroundColor: 'purple',
        color: 'brown',
        fontSize: 20,
        width: 35
    },
    container: {
        padding: 10
    },
    image: {
        width: 300,
        height: 150,
        resizeMode: 'cover'
    },
    drawerStyles: {
        drawer: {
            shadowColor: '#000000',
            shadowOpacity: 0.8,
            shadowRadius: 3
        },
        main: {
            paddingLeft: 3
        }
    }
}



// <Scene key="drawer" component={MainDrawer}>
//     <Scene key="home" component={Home} title="Home" />
//     <Scene key="profile" component={Profile} title="Profile"></Scene>
// </Scene>

// <Drawer
//     ref={(ref) => this._drawer = ref}
//     content={<SideMenu closeMenu={this._closeControlPanel}/>}>
//     <Home
//         openMenu={this._openControlPanel}
//     />
// </Drawer>

// <Image source={require('./img/backdrop.jpeg')} resizeMode="cover">
//
// </Image>

// <Container>
//     <Header>
//         <Button transparent>
//             <Icon name="ios-arrow-left"/>
//         </Button>
//         <Title>Header</Title>
//         <Button transparent>
//             <Icon name="navicon"/>
//         </Button>
//     </Header>
//     <Content style={styles.container}>
//         <InputGroup borderType="rounded" placeholder="Username">
//             <Icon name="ios-home" style={{color: '#345edf'}} />
//             <Input/>
//         </InputGroup>
//         <InputGroup borderType="rounded" placeholder="Password" secureTextEntry={true}>
//             <Icon name="ios-home" style={{color: '#345edf'}} />
//             <Input/>
//         </InputGroup>
//         <List>
//             <ListItem>
//                 <Thumbnail square source={require('./img/circles.jpg')} />
//                 <Text>GoalKeeper</Text>
//                 <Text note>Softly</Text>
//             </ListItem>
//             <ListItem iconLeft>
//                 <Icon name="ios-alarm" />
//                 <Text>GoalKeeper</Text>
//                 <Badge primary>2</Badge>
//             </ListItem>
//             <ListItem iconLeft>
//                 <Icon name="ios-bell" />
//                 <Text>GoalKeeper</Text>
//                 <Text note>Side note</Text>
//             </ListItem>
//         </List>
//         <Text>This is Text</Text>
//         <Card>
//             <CardItem header>
//                 <Text>Header</Text>
//             </CardItem>
//             <CardItem cardBody>
//                 <Thumbnail source={require('./img/circles.jpg')}/>
//                 <Text>Instrumental Songs</Text>
//                 <Text note>
//                     Guitar
//                 </Text>
//             </CardItem>
//             <CardItem>
//                 <Image
//                     style={styles.image}
//                     source={require('./img/jumbotron.jpg')}/>
//                 <Text>This is the summary of all that you have been doing in the last 10 years of your life. Do you have an app that u built?</Text>
//                 <Button transparent>
//                     <Icon name="social-facebook" />
//                     398 stars
//                 </Button>
//             </CardItem>
//             <CardItem>
//                 <Icon name="ios-musical-notes" style={{color: '#ed4a6a'}} />
//                 <Text>Listen now</Text>
//             </CardItem>
//             <CardItem>
//                 <Icon name="social-google" />
//                 <Text>
//                     Holla!!
//                     Instead of showing the names of authors as a list, we have put them in a swiper which gives each wallpaper its very own screen, which we can swipe across.
//                 </Text>
//             </CardItem>
//             <CardItem>
//                 <Icon name="social-facebook" />
//                 <Text>
//                     Holla!!
//                     Instead of showing the names of authors as a list, we have put them in a swiper which gives each wallpaper its very own screen, which we can swipe across.
//                 </Text>
//             </CardItem>
//             <CardItem>
//                 <Icon name="social-twitter" />
//                 <Text>
//                     Holla!!
//                     Instead of showing the names of authors as a list, we have put them in a swiper which gives each wallpaper its very own screen, which we can swipe across.
//                 </Text>
//             </CardItem>
//             <CardItem footer>
//                 <Text>Footer</Text>
//             </CardItem>
//         </Card>
//         <Badge style={styles.badge}>2</Badge>
//         <Badge primary>2</Badge>
//         <Badge success>2</Badge>
//         <Badge info>2</Badge>
//         <Badge warning>2</Badge>
//         <Badge danger>2</Badge>
//         <Button rounded>Login</Button>
//         <Button rounded block>Login</Button>
//         <Button>
//             <Icon name="ios-home"/>
//             Login
//         </Button>
//         <Button warning bordered>Login</Button>
//     </Content>
//     <Footer>
//         <Button transparent>
//             <Icon name="ios-telephone" />
//         </Button>
//         <Title>Footer</Title>
//         <Button transparent>
//             <Icon name="chatbox" />
//         </Button>
//     </Footer>
// </Container>