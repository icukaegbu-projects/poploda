/**
 * Created by ikedi on 23/05/2016.
 */
import React, {Component} from 'react';
import {
    Container,
    Content,
    Header,
    Footer,
    Icon,
    Text,
    InputGroup,
    Input,
    Button
} from 'native-base';
import {Image, View, Dimensions} from 'react-native';
import Banner from './Banner';
import {Actions} from 'react-native-router-flux';

const ScreenHeight = Dimensions.get("window").height;

export default class ForgotPassword extends Component{
    render(){
        return (
                <Container style={styles.container}>
                    <Header style={styles.header}>
                        <Image
                            style={styles.image}
                            source={require('../../img/logo3.png')}/>
                    </Header>
                    <Content>
                        <Text style={styles.centerText}>Enter Your Email To Recover Your Password</Text>
                        <View style={styles.content}>
                            <InputGroup placeholder="Email">
                                <Icon name={this.props.renderIcon("mail")} size={30} color="#900" />
                                <Input/>
                            </InputGroup>
                            <Button block style={styles.button}>
                                Recover Password
                            </Button>
                        </View>
                    </Content>
                    <Footer style={styles.footer}>
                        <Text style={{fontSize:12,color:'#BB77FF',paddingBottom:20}}>Already have an account? </Text>
                        <Text></Text>
                        <Button small transparent
                                textStyle={styles.linkButton}
                                onPress={Actions.pop}>
                            Sign In
                        </Button>
                    </Footer>
                </Container>
        )
    }
}

let styles = {
    container: {
        flex: 1,
        backgroundColor: 'transparent',
        flexDirection: 'column'
    },
    header: {
        flex: 2,
        backgroundColor: '#BB77FF',
        height: 250
    },
    content: {
        flex: 1,
        margin: 20,
        paddingBottom: 0,
        // borderWidth: 1,
        // borderColor: 'grey',
        backgroundColor: 'white'
    },
    footer: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'transparent',
        paddingLeft: 20,
        paddingRight: 10
    },
    image: {
        resizeMode:"contain",
        width: 200,
        height: 200
    },
    centerText: {
        backgroundColor: '#BB77FF',
        color: 'white',
        fontSize: 16,
        padding: 20,
        flexDirection: 'column',
        justifyContent: 'center',
        alignItems: 'center'
    },
    button: {
        backgroundColor: '#BB77FF',
        marginBottom: 0,
        borderRadius: 0
    },
    linkButton: {
        fontSize: 15,
        color: '#BB77FF',
        fontWeight: '900',
        borderColor: '#BB77FF',
        borderWidth: 1,
        borderRadius: 5,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 5,
        paddingLeft: 10
    }
}