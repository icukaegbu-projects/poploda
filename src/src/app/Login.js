/**
 * Created by ikedi on 23/05/2016.
 */
import React, {Component} from 'react';
import {
    Container,
    Content,
    Header,
    Footer,
    Icon,
    Text,
    InputGroup,
    Input,
    Button,
    Spinner
} from 'native-base';
import {Image, View, Dimensions} from 'react-native';
import {Actions} from 'react-native-router-flux';
import GiftedSpinner from 'react-native-gifted-spinner';

import Authentication from '../api/Authentication';

const ScreenHeight = Dimensions.get("window").height;

export default class Login extends Component{
    constructor(props){
        super(props);
        //this.appUrl = props.appUrl;
        this.state = {
            username: '',
            password: '',
            loggedIn: false,
            loginButtonClicked: false,
            showSpinner: false
        }

        //setup the Authentication object
        //Authentication.init(props.appUrl);

        this._signIn = this._signIn.bind(this);
        this._focusNextField = this._focusNextField.bind(this);
    }

    _focusNextField(nextField) {
        this.refs[nextField].focus()
    }

    _signIn(){
       //for now, connect to firebase and write the username and data to it
       //  this.appUrl.set({
       //      username: this.state.username,
       //      password: this.state.password
       //  });
        console.log('Routing to home page')
        //if connection successful, display Home page
        //else display error and login page again
        //Actions.drawer();
        //Actions.home();

        //flip the loginButtonClicked variable
        this.setState({loginButtonClicked: !this.state.loginButtonClicked})
        //this.setState({showSpinner: !this.state.showSpinner})
        if (Authentication.login(this.state.username, this.state.password)){
            this.setState({loggedIn: true})
            //this.setState({showSpinner: !this.state.showSpinner})
        }

        //LOGIN FAILED, HIDE SPINNER
        //this.setState({loginButtonClicked: !this.state.loginButtonClicked})
        //this.setState({showSpinner: !this.state.showSpinner})

        //this.state.showSpinner
    }

    render(){
        return (
            <Image
                source={require('../../img/backdrop2.jpeg')}
                style={styles.coverImage}>
                <Container style={styles.container}>
                    <Header style={styles.header}>
                        <Image
                            style={styles.image}
                            source={require('../../img/logo3.png')}/>
                    </Header>
                    <Content>
                        {
                            (this.state.loginButtonClicked && !this.state.loggedIn) ?
                                <Spinner color="white" ref="spinner"/> : <Text>''</Text>
                        }
                        <View style={styles.content}>
                            <InputGroup placeholder="Email">
                                <Icon name={this.props.renderIcon("person")} color="#900" size={30}/>
                                <Input
                                    ref="email"
                                    value={this.state.username}
                                    onChangeText={(username) => this.setState({username})}
                                    autoFocus={true}
                                    returnKeyType="next"
                                    keyboardType="email-address"
                                    onSubmitEditing={() => this._focusNextField('password')}
                                    />
                            </InputGroup>
                            <InputGroup  placeholder="Password" secureTextEntry={true}>
                                <Icon name={this.props.renderIcon("lock")} color="#900" size={30}/>
                                <Input
                                    ref="password"
                                    value={this.state.password}
                                    onChangeText={(password) => this.setState({password})}
                                    returnKeyType="done"/>
                            </InputGroup>
                            <Button block
                                    style={styles.button}
                                    disabled={(!this.state.username || !this.state.password) ? true : false}
                                    onPress={this._signIn}>
                                Sign In
                            </Button>
                        </View>
                    </Content>
                    <Footer style={styles.footer}>
                        <Button small transparent
                                textStyle={styles.linkButton}
                                onPress={Actions.signup}>
                            Sign Up
                        </Button>
                        <Text></Text>
                        <Button small transparent
                                textStyle={styles.linkButton}
                                onPress={Actions.forgotPassword}>
                            Forgot Password
                        </Button>
                    </Footer>
                </Container>
            </Image>

        )
    }
}

let styles = {
    container: {
        flex: 1,
        backgroundColor: 'transparent',
        flexDirection: 'column'
    },
    header: {
        flex: 2,
        backgroundColor: 'transparent',
        height: 320
    },
    content: {
        flex: 1,
        margin: 20,
        paddingBottom: 0,
        borderWidth: 1,
        borderColor: 'grey',
        backgroundColor: 'white'
    },
    footer: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'transparent'
    },
    image: {
        resizeMode:"contain",
        width: 200,
        height: 200
    },
    coverImage: {
        resizeMode:"cover",
        width: null,
        height: ScreenHeight
        // justifyContent: 'center',
        // alignItems: 'center'
    },
    button: {
        backgroundColor: '#BB77FF',
        marginBottom: 0,
        borderRadius: 0
    },
    linkButton: {
        fontSize: 15,
        color: 'white',
        padding: 10
    }
}