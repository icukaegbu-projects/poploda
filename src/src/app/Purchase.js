/**
 * Created by ikedi on 02/06/2016.
 */
import React, {Component} from 'react';
import {
    Container,
    Header,
    Content,
    List,
    ListItem,
    Text,
    Icon,
    Title,
    Button,
    Card,
    CardItem,
    Thumbnail,
    InputGroup,
    Input,
    Picker,
    Switch
} from 'native-base';
import { Image, Dimensions } from 'react-native';
import { SegmentedControls } from 'react-native-radio-buttons';
import HeaderButton from './core/HeaderButton';
import DatePicker from 'react-native-datepicker';
//import Switch from 'react-native';

const ScreenHeight = Dimensions.get("window").height;
let ScreenWidth = Dimensions.get("window").width;
ScreenWidth = ScreenWidth * 0.83;

export default class Purchase extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedOption: 1
        }
    }

    setSelectedOption(selectedOption){
        this.setState({
            selectedOption
        });
    }
    render(){
        const options = [
            "Airtel",
            "Etisalat",
            "Glo",
            "MTN"
        ];
        return (
            <Container style={styles.container}>
                <Header style={styles.header}>
                    <HeaderButton
                        openMenu={this.props.openMenu}
                        renderIcon={this.props.renderIcon}/>
                    <Image
                        style={styles.image}
                        source={require('../../img/logo3.png')}/>
                </Header>
                <Content style={styles.content}>
                    <Card>
                        <CardItem header>
                            <Text>Buy Airtime</Text>
                        </CardItem>
                        <CardItem>
                            <Text note>Choose Network</Text>
                            <SegmentedControls
                                options={ options }
                                onSelection={ this.setSelectedOption.bind(this) }
                                selectedOption={ this.state.selectedOption }

                            />
                        </CardItem>
                        <CardItem>
                            <Text note>Choose Amount</Text>
                            <InputGroup borderType="rounded" placeholder="Amount">
                                <Icon name={this.props.renderIcon("cash")} color="#900" size={30}/>
                                <Input type="number"/>
                            </InputGroup>
                            <InputGroup borderType="rounded" placeholder="Phone">
                                <Icon name={this.props.renderIcon("phone-portrait")} color="#900" size={30}/>
                                <Input type="tel"/>
                            </InputGroup>

                        </CardItem>

                        <CardItem>
                            <Text note>Make Payment</Text>
                            <Text>From Wallet</Text>
                            <Switch style={{marginBottom: 10}}/>

                            <Text>Registered Card</Text>
                            <Switch style={{marginBottom: 10}} value={true}/>

                            <Text>New Card Details</Text>
                            <InputGroup borderType="rounded" placeholder="Card Number">
                                <Icon name={this.props.renderIcon("card")} size={30} color="#900" />
                                <Input type="number"/>
                            </InputGroup>
                            <InputGroup borderType="rounded" placeholder="CVV" secureTextEntry={true}>
                                <Icon name={this.props.renderIcon("lock")} color="#900" size={30}/>
                                <Input type="number"/>
                            </InputGroup>
                            <InputGroup borderType="rounded" placeholder="Card PIN" secureTextEntry={true}>
                                <Icon name={this.props.renderIcon("key")} color="#900" size={30}/>
                                <Input type="number"/>
                            </InputGroup>
                            <Text note>Expiry Date</Text>
                            <DatePicker
                                mode="date"
                                style={styles.datePicker}
                                date={this.state.selectedDate}
                                format="YYYY-MM"
                                minDate="2016-05-1"
                                maxDate="2022-12-31"
                                confirmBtnText="Confirm"
                                cancelBtnText="Cancel"
                                onDateChange={(selectedDate) => {this.setState({selectedDate})}}
                            />
                        </CardItem>
                        <CardItem>
                            <Text note>Confirm Purchase</Text>
                            <InputGroup borderType="rounded" placeholder="Transaction PIN" secureTextEntry={true}>
                                <Icon name={this.props.renderIcon("key")} color="#900" size={30}/>
                                <Input type="number"/>
                            </InputGroup>
                        </CardItem>
                        <CardItem>
                            <Button rounded block style={styles.button}>
                                Buy Credit
                            </Button>
                        </CardItem>
                    </Card>
                </Content>
            </Container>
        )
    }
}

let styles = {
    container: {
        flex: 1,
        backgroundColor: 'transparent',
        flexDirection: 'column'
    },
    header: {
        flex: 2,
        backgroundColor: '#BB77FF'
    },
    content: {
        flex: 1,
        margin: 10,
        paddingBottom: 0,
        // borderWidth: 1,
        // borderColor: 'grey',
        backgroundColor: 'white'
    },
    footer: {
        flex: 1,
        backgroundColor: 'transparent'
    },
    image: {
        resizeMode:"contain",
        width: 40,
        height: 40
    },
    coverImage: {
        resizeMode:"cover",
        width: null,
        height: ScreenHeight
        // justifyContent: 'center',
        // alignItems: 'center'
    },
    button: {
        backgroundColor: '#BB77FF',
        marginBottom: 0,
        borderRadius: 0
    },
    linkButton: {
        fontSize: 15,
        color: '#BB77FF',
        fontWeight: '900',
        borderColor: '#BB77FF',
        borderWidth: 1,
        borderRadius: 5,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 5,
        paddingLeft: 10
    },
    datePicker: {
        fontSize: 15,
        color: '#BB77FF',
        fontWeight: '900',
        // borderColor: '#BB77FF',
        borderColor: '#000',
        borderWidth: 1,
        borderRadius: 10,
        paddingTop: 3,
        paddingRight: 3,
        paddingBottom: 3,
        paddingLeft: 3,
        marginLeft: 3,
        marginRight: 10,
        width: ScreenWidth
    }
}

// let styles = {
//     background: {
//         backgroundColor: 'royalblue'
//     },
//     container: {
//         padding: 20
//     },
//     content: {
//         padding: 20
//     },
//     image: {
//         width: 335,
//         height: 300
//     },
//     button: {
//         backgroundColor: 'rgba(255, 0, 31, 0.78)'
//     },
//     headerText: {
//         paddingTop: 5,
//         fontSize: 20,
//         color: 'antiquewhite'
//     }
// }