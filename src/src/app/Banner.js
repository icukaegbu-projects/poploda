/**
 * Created by ikedi on 14/05/2016.
 */
/**
 * Created by ikedi on 14/05/2016.
 */
import React, {Component} from 'react';
//import {Grid, Col, Row} from 'react-native-easy-grid';
import {
    Header,
    Text,
    Icon,
    Button
} from 'native-base';
import {Image, View} from 'react-native';

export default class Banner extends Component{
    constructor(props){
        super(props);
    }
    render(){
        return (
            <View>
                <Button transparent
                        onPress={this.props.openMenu}>
                    <Icon name="android-menu" style={{color: '#BA77FF'}} />
                </Button>
                <Image
                    style={styles.image}
                    source={require('../../img/logo3.png')}/>
            </View>
        );
    }
}

let styles = {
    background: {
        // backgroundColor: 'royalblue'
        backgroundColor: '#BA77FF'
    },
    container: {
        padding: 20
    },
    header: {
        backgroundColor: '#FFFFFF'
    },
    content: {
        padding: 20
    },
    image: {
        width: 335,
        height: 300
    },
    button: {
        backgroundColor: 'rgba(255, 0, 31, 0.78)'
    },
    headerText: {
        paddingTop: 5,
        fontSize: 20,
        color: '#BA77FF'
    }
}
