/**
 * Created by ikedi on 23/05/2016.
 */
import React, {Component} from 'react';
import {
    Container,
    Content,
    Header,
    Footer,
    Icon,
    Text,
    InputGroup,
    Input,
    Button
} from 'native-base';
import {Image, View, Dimensions} from 'react-native';
import { Actions } from 'react-native-router-flux';
import HeaderButton from './core/HeaderButton';

const ScreenHeight = Dimensions.get("window").height;

export default class Wallet extends Component{
    constructor(props){
        super(props)
    }
    render(){
        return (
            <Container style={styles.container}>
                <Header style={styles.header}>
                    <HeaderButton
                        openMenu={this.props.openMenu}
                        renderIcon={this.props.renderIcon}/>
                    <Image
                        style={styles.image}
                        source={require('../../img/logo3.png')}/>

                </Header>
                <Content>
                    <View style={styles.content}>
                        <View style={styles.section}>
                            <Text note>My Wallet</Text>
                        </View>


                    </View>
                </Content>
                <Footer style={styles.footer}>
                    <Text style={{fontSize:12,color:'#BB77FF'}}>Load More? </Text>
                    <Text></Text>
                    <Button
                        small transparent
                        textStyle={styles.linkButton}
                        onPress={Actions.login}>
                        Load More
                    </Button>
                </Footer>
            </Container>
        )
    }
}

let styles = {
    container: {
        flex: 1,
        backgroundColor: 'transparent',
        flexDirection: 'column'
    },
    header: {
        flex: 2,
        backgroundColor: '#BB77FF'
    },
    content: {
        flex: 1,
        margin: 20,
        paddingBottom: 0,
        // borderWidth: 1,
        // borderColor: 'grey',
        backgroundColor: 'white'
    },
    section: {
        paddingTop: 10
    },
    footer: {
        flex: 1,
        backgroundColor: 'transparent'
    },
    image: {
        resizeMode:"contain",
        width: 40,
        height: 40
    },
    coverImage: {
        resizeMode:"cover",
        width: null,
        height: ScreenHeight
        // justifyContent: 'center',
        // alignItems: 'center'
    },
    button: {
        backgroundColor: '#BB77FF',
        marginBottom: 0,
        borderRadius: 0
    },
    linkButton: {
        fontSize: 15,
        color: '#BB77FF',
        fontWeight: '900',
        borderColor: '#BB77FF',
        borderWidth: 1,
        borderRadius: 5,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 5,
        paddingLeft: 10
    }
}

// <Button transparent>
//     <Icon name="back"/>
// </Button>