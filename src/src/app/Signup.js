/**
 * Created by ikedi on 23/05/2016.
 */
import React, {Component} from 'react';
import {
    Container,
    Content,
    Header,
    Footer,
    Icon,
    Text,
    InputGroup,
    Input,
    Button
} from 'native-base';
import {Image, View, Dimensions} from 'react-native';
import Banner from './Banner';
import {Actions} from 'react-native-router-flux';
import Authentication from '../api/Authentication';
import Message from '../api/Message'

const ScreenHeight = Dimensions.get("window").height;

export default class Signup extends Component{
    constructor(props){
        super(props);

        this.state = {
            email: '',
            fullName: '',
            password: '',
            passwordConfirm: ''
        }

        this._signup = this._signup.bind(this);
    }

    _signup(){
        //check content of fields are not empty
        //then pass details to signup method
        if (this.state.email === '' || this.state.password === '' ||
            this.state.passwordConfirm === '') {
            Message.displayMessage('Email & Password cannot be null');
            return;
        }

        if (this.state.password !== this.state.passwordConfirm){
            Message.displayMessage('Password does not match');
            this.setState({passwordConfirm: ''});
            return;
        }

        Authentication.signup(this.state.email, this.state.password,
            this.state.fullName);
    }

    render(){
        return (
                <Container style={styles.container}>
                    <Header style={styles.header}>
                        <Image
                            style={styles.image}
                            source={require('../../img/logo3.png')}/>
                    </Header>
                    <Content>
                        <View style={styles.content}>
                            <InputGroup
                                placeholder="Full Name"
                                onChangeText={(fullName) => this.setState({fullName})}
                                value={this.state.fullName}>
                                <Icon name={this.props.renderIcon("person")} size={30} color="#900" />
                                <Input/>
                            </InputGroup>
                            <InputGroup
                                placeholder="Email"
                                onChangeText={(email) => this.setState({email})}
                                value={this.state.email}>
                                <Icon name={this.props.renderIcon("mail")} size={30} color="#900" />
                                <Input/>
                            </InputGroup>
                            <InputGroup
                                placeholder="Password"
                                secureTextEntry={true}
                                onChangeText={(password) => this.setState({password})}
                                value={this.state.password}>
                                <Icon name={this.props.renderIcon("lock")} color="#900" size={30}/>
                                <Input/>
                            </InputGroup>
                            <InputGroup
                                placeholder="Confirm Password"
                                secureTextEntry={true}
                                onChangeText={(passwordConfirm) => this.setState({passwordConfirm})}
                                value={this.state.passwordConfirm}>
                                <Icon name={this.props.renderIcon("lock")} color="#900" size={30}/>
                                <Input/>
                            </InputGroup>
                            <Button
                                block
                                style={styles.button}
                                onPress={this._signup}>
                                Sign Up
                            </Button>
                        </View>
                    </Content>
                    <Footer style={styles.footer}>
                        <Text style={{fontSize:12,color:'#BB77FF',paddingBottom:20}}>Already have an account? </Text>
                        <Text></Text>
                        <Button small transparent
                                textStyle={styles.linkButton}
                                onPress={Actions.pop}>
                            Sign In
                        </Button>
                    </Footer>
                </Container>
        )
    }
}

let styles = {
    container: {
        flex: 1,
        backgroundColor: 'transparent',
        flexDirection: 'column'
    },
    header: {
        flex: 2,
        backgroundColor: '#BB77FF',
        height: 250
    },
    content: {
        flex: 1,
        margin: 20,
        paddingBottom: 0,
        // borderWidth: 1,
        // borderColor: 'grey',
        backgroundColor: 'white'
    },
    footer: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'transparent',
        paddingLeft: 20,
        paddingRight: 10
    },
    image: {
        resizeMode:"contain",
        width: 200,
        height: 200
    },
    coverImage: {
        resizeMode:"cover",
        width: null,
        height: ScreenHeight
        // justifyContent: 'center',
        // alignItems: 'center'
    },
    button: {
        backgroundColor: '#BB77FF',
        marginBottom: 0,
        borderRadius: 0
    },
    linkButton: {
        fontSize: 15,
        color: '#BB77FF',
        fontWeight: '900',
        borderColor: '#BB77FF',
        borderWidth: 1,
        borderRadius: 5,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 5,
        paddingLeft: 10
    }
}