/**
 * Created by ikedi on 20/05/2016.
 */
import React, { Component } from 'react';
import {
    Container,
    Content,
    List,
    ListItem,
    Button,
    Header,
    Text,
    Icon
} from 'native-base';
import { Actions } from 'react-native-router-flux';
import Authentication from '../../api/Authentication';

export default class SideMenu extends Component {
    constructor(props){
        super(props);

        this._logout = this._logout.bind(this);
        this._loadPage = this._loadPage.bind(this);
        this._home = this._home.bind(this);
        this._profile = this._profile.bind(this);
        this._purchase = this._purchase.bind(this);
        this._transactions = this._transactions.bind(this);
        this._wallet = this._wallet.bind(this);
        this._cards = this._cards.bind(this);
    }

    _logout() {
        //disconnect from Firebase
        // hide the Menubar
        this.props.closeMenu();

        Authentication.logout();

        // and route back to login page
        Actions.login();
    }

    _home() {
        this.props.closeMenu();
        Actions.home();
    }

    _profile() {
        this.props.closeMenu();
        Actions.profile();
    }

    _wallet() {
        this.props.closeMenu();
        Actions.wallet();
    }

    _purchase() {
        this.props.closeMenu();
        Actions.purchase();
    }

    _transactions() {
        this.props.closeMenu();
        Actions.transactions();
    }

    _cards() {
        this.props.closeMenu();
        Actions.cards();
    }

    _loadPage(page) {

        switch (page) {
            case page === 'home':
                console.log(page);
                Actions.home();
                break;
            case page === 'profile':
                Actions.profile();
                break;
            case page === 'wallet':
                return Actions.wallet();
                break;
            case page === 'purchase':
                Actions.purchase();
                break;
            case page === 'transactions':
                Actions.transactions();
                break;
            case page === 'cards':
                Actions.cards();
                break;
        }

        //close the menu
        this.props.closeMenu();
    }

    render(){
        //USE A TEST TO CHECK PLATFORM AND RENDER
        return (
            <Container>
                <Header style={styles.header}>
                    <Button small transparent
                            onPress={this.props.closeMenu}>
                        <Icon name={this.props.renderIcon("close")} />
                    </Button>
                    <Text style={styles.headerText}>Menu</Text>
                </Header>
                <Content style={styles.container}>
                    <List>
                        <ListItem key="home">
                            <Button 
                                transparent 
                                textStyle={styles.button}
                                onPress={this._home}>
                                <Icon name={this.props.renderIcon("home")} style={styles.icon}/>
                                Home
                            </Button>
                        </ListItem>
                        <ListItem key="purchase">
                            <Button transparent
                                    textStyle={styles.button}
                                    onPress={this._purchase}>
                                <Icon name={this.props.renderIcon("cash")} style={styles.icon}/>
                                Buy Credit
                            </Button>
                        </ListItem>
                        <ListItem key="cards">
                            <Button transparent
                                    textStyle={styles.button}
                                    onPress={this._cards}>
                                <Icon name={this.props.renderIcon("card")} style={styles.icon}/>
                                Debit Cards
                            </Button>
                        </ListItem>
                        <ListItem key="transactions">
                            <Button transparent
                                    textStyle={styles.button}
                                    onPress={this._transactions}>
                                <Icon name={this.props.renderIcon("information")} style={styles.icon}/>
                                Transaction History
                            </Button>
                        </ListItem>
                        <ListItem key="wallet">
                            <Button transparent
                                    textStyle={styles.button}
                                    onPress={this._wallet}>
                                <Icon name={this.props.renderIcon("briefcase")} style={styles.icon}/>
                                My Wallet
                            </Button>
                        </ListItem>
                        <ListItem key="profile">
                            <Button transparent
                                    textStyle={styles.button}
                                    onPress={this._profile}>
                                <Icon name={this.props.renderIcon("person")} style={styles.icon}/>
                                My Profile
                            </Button>
                        </ListItem>
                        <ListItem key="logout">
                            <Button transparent
                                    textStyle={styles.button}
                                    onPress={this._logout}>
                                <Icon name={this.props.renderIcon("log-out")} style={styles.button}/>
                                Logout
                            </Button>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        )
    }
}

let styles = {
    container: {
        padding: 20,
        backgroundColor: '#BB77FF'
    },
    button: {
        color: '#FFFFFF',
        marginLeft: 10,
        fontSize: 15,
        padding: 2
    },
    icon: {
        color: '#FFFFFF'
    },
    header: {
        backgroundColor: '#BB77FF'
    },
    headerText: {
        paddingTop: 5,
        fontSize: 20,
        color: 'antiquewhite'
    }
}