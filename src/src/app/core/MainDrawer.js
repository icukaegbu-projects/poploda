/**
 * Created by ikedi on 29/05/2016.
 */
import React, {Component} from 'react';
import Drawer from 'react-native-drawer';
import { DefaultRenderer } from 'react-native-router-flux';
import SideMenu from './SideMenu';

export default class MainDrawer extends Component {
    constructor(props) {
        super(props);

        //bind methods to this
        this._closeSideMenu = this._closeSideMenu.bind(this);
        this._openSideMenu = this._openSideMenu.bind(this);
    }

    _openSideMenu() {
        this._drawer.open();
    }

    _closeSideMenu() {
        this._drawer.close();
    }

    render(){
        const children = this.props.navigationState.children;

        return (
            <Drawer
                ref={(ref) => this._drawer = ref}
                styles={styles.drawerStyles}
                type="overlay"
                tapToClose={true}
                openDrawerOffset={0.2}
                content={<SideMenu closeMenu={this._closeSideMenu}/>}>

                <DefaultRenderer navigationState={children[0]} />
             </Drawer>
        )
    }
}

let styles = {
    drawerStyles: {
        drawer: {
            shadowColor: '#000000',
            shadowOpacity: 0.8,
            shadowRadius: 3
        },
        main: {
            paddingLeft: 3
        }
    }
}
