/**
 * Created by ikedi on 25/05/2016.
 */
import React, {Component} from 'react';
import {Button, Icon} from 'native-base';

export default class HeaderButton extends Component {
    constructor(props){
        super(props);
    }
    render(){
        return (
            <Button transparent
                    onPress={this.props.openMenu}>
                <Icon name={this.props.renderIcon("menu")} style={{color: '#FFFFFF'}} />
            </Button>
        )
    }
}