/**
 * Created by ikedi on 20/06/2016.
 */
import React, {Component} from 'react';
import {
    ListItem,
    Thumbnail,
    Text,
    Badge
} from 'native-base';
import { View } from 'react-native';

export default class TransactionItem extends Component {
    constructor(props){
        super(props);
        this.AIRTEL = '../../img/Airtel.png';
        this.ETISALAT = '../../img/Etisalat.jpg';
        this.GLO = '../../img/Glo.jpg';
        this.MTN = '../../img/MTN.jpg';
        this.defaultNetwork = '../../img/logo3.png';

        this.state = {
            selectedNetwork: this.defaultNetwork
        }

        this._buildThumbnail = this._buildThumbnail.bind(this);
        this._buildStyle = this._buildStyle.bind(this);
    }

    componentDidMount(){
        // if(this.props.network === 'AIRTEL'){
        //     this.setState({selectedNetwork: this.AIRTEL});
        // }else if(this.props.network === 'ETISALAT') {
        //     this.setState({selectedNetwork: this.ETISALAT});
        // }else if(this.props.network === 'GLO') {
        //     this.setState({selectedNetwork: this.GLO});
        // }else if(this.props.network === 'MTN'){
        //     this.setState({selectedNetwork: this.MTN})
        // }else {
        //     this.setState({selectedNetwork: this.defaultNetwork})
        // }

    }

    _buildThumbnail() {
        if(this.props.network === 'AIRTEL'){
            return <Thumbnail source={require('../../img/Airtel.png')} />
        }else if(this.props.network === 'ETISALAT') {
            return <Thumbnail source={require('../../img/Etisalat.jpg')} />
        }else if(this.props.network === 'GLO') {
            return <Thumbnail source={require('../../img/Glo.jpg')} />
        }else if(this.props.network === 'MTN'){
            return <Thumbnail source={require('../../img/MTN.jpg')} />
        }else {
            return <Thumbnail source={require('../../img/logo3.png')} />
        }
    }

    _buildStyle() {
        if(this.props.network === 'AIRTEL'){
            return styles.AIRTEL;
        }else if(this.props.network === 'ETISALAT') {
            return styles.ETISALAT;
        }else if(this.props.network === 'GLO') {
            return styles.GLO;
        }else if(this.props.network === 'MTN'){
            return styles.MTN;
        }else {
            return styles.DEFAULT;
        }
    }

    render() {
        return (
            <ListItem>
                {this._buildThumbnail()}
                <View>
                    <Text style={[this._buildStyle(), styles.phone]}>Phone Number</Text>
                    <Text>#1,200</Text>
                </View>
                <Text note>Date purchased</Text>
            </ListItem>
        )
    }
}

let styles = {
    AIRTEL: {
        color: 'red'
    },
    ETISALAT: {
        color: 'green'
    },
    GLO: {
        color: 'limegreen'
    },
    MTN: {
        color: 'yellow'
    },
    DEFAULT: {
        color: 'white'
    },
    phone: {
        fontWeight: '700',
        fontSize: 20
    }
}