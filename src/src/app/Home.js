/**
 * Created by ikedi on 15/05/2016.
 */
import React, {Component} from 'react';
import {
    Container,
    Content,
    Header,
    Footer,
    Icon,
    Text,
    InputGroup,
    Input,
    Button,
    Card,
    CardItem,
    Thumbnail,
    List,
    ListItem
} from 'native-base';
import {Image, View, Dimensions} from 'react-native';
import { Actions } from 'react-native-router-flux';
import HeaderButton from './core/HeaderButton';
import TransactionItem from './TransactionItem';

const ScreenHeight = Dimensions.get("window").height;
const chartData = [
    {
        name: 'BarChart',
        type: 'bar',
        color:'purple',
        widthPercent: 0.6,
        data: [30, 1, 1, 2, 3, 5, 21, 13, 21, 34, 55, 30],
    },
    {
        name: 'LineChart',
        color: 'gray',
        lineWidth: 2,
        highlightIndices: [1, 2],   // The data points at indexes 1 and 2 will be orange
        highlightColor: 'orange',
        showDataPoint: true,
        data: [10, 12, 14, 25, 31, 52, 41, 31, 52, 66, 22, 11],
    }
];

const xLabels = ['0','1','2','3','4','5','6','7','8','9','10','11'];

export default class Home extends Component{
    constructor(props){
        super(props)
    }
    render(){
        return (
            <Container style={styles.container}>
                <Header style={styles.header}>
                    <HeaderButton
                        openMenu={this.props.openMenu}
                        renderIcon={this.props.renderIcon}/>
                    <Image
                        style={styles.image}
                        source={require('../../img/logo3.png')}/>

                </Header>
                <Content>
                    <Card>
                        <CardItem header>
                            <Thumbnail source={require('../../img/logo2.png')}/>
                            <Text>Home</Text>
                        </CardItem>
                        <CardItem>
                            <Text note>Wallet</Text>
                            <Text>Current Balance:</Text>
                            <Text style={styles.bigText}>12,500</Text>
                            <Button style={styles.button}>Fund Wallet</Button>
                        </CardItem>
                        <CardItem>
                            <Text note>Last Purchase</Text>
                            <List>
                                <TransactionItem network="AIRTEL" style={styles.linkButton}/>
                            </List>
                            <Text note>Total Purchase: Last 7 days</Text>
                            <List>
                                <TransactionItem network="AIRTEL"/>
                                <TransactionItem network="ETISALAT"/>
                                <TransactionItem network="GLO"/>
                                <TransactionItem network="MTN"/>
                            </List>
                        </CardItem>
                    </Card>
                </Content>
                <Footer style={styles.footer}>
                    <Text style={{fontSize:12,color:'#BB77FF',paddingBottom:20}}>Load More? </Text>
                    <Text></Text>
                    <Button
                        small transparent
                        textStyle={styles.linkButton}
                        onPress={Actions.login}>
                        Load More
                    </Button>
                </Footer>
            </Container>
        )
    }
}

let styles = {
    container: {
        flex: 1,
        backgroundColor: 'transparent',
        flexDirection: 'column'
    },
    header: {
        flex: 2,
        backgroundColor: '#BB77FF'
    },
    content: {
        flex: 1,
        margin: 20,
        paddingBottom: 0,
        // borderWidth: 1,
        // borderColor: 'grey',
        backgroundColor: 'white'
    },
    section: {
        paddingTop: 10
    },
    footer: {
        flex: 1,
        flexDirection: 'row',
        backgroundColor: 'transparent',
        paddingLeft: 20,
        paddingRight: 10
    },
    image: {
        resizeMode:"contain",
        width: 40,
        height: 40
    },
    coverImage: {
        resizeMode:"cover",
        width: null,
        height: ScreenHeight
        // justifyContent: 'center',
        // alignItems: 'center'
    },
    button: {
        backgroundColor: '#BB77FF',
        marginBottom: 0,
        borderRadius: 0
    },
    linkButton: {
        fontSize: 15,
        color: '#BB77FF',
        fontWeight: '900',
        borderColor: '#BB77FF',
        borderWidth: 1,
        borderRadius: 5,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 5,
        paddingLeft: 10
    },
    chart: {
        // position: 'absolute',
        // top: 16,
        // left: 4,
        // bottom: 4,
        // right: 16,
        width: 300,
        height: 300
    },
    headerText: {
        paddingTop: 5,
        fontSize: 20,
        color: '#BA77FF'
    },
    bigText: {
        fontSize: 30,
        fontWeight: '900',
        paddingTop:20
    }
}

// <Button transparent>
//     <Icon name="back"/>
// </Button>


// let styles = {
//     background: {
//         // backgroundColor: 'royalblue'
//         backgroundColor: '#BA77FF'
//     },
//     container: {
//         padding: 20
//     },
//     header: {
//         backgroundColor: '#FFFFFF'
//     },
//     content: {
//         padding: 20
//     },
//     image: {
//         width: 335,
//         height: 300
//     },
//     button: {
//         backgroundColor: 'rgba(255, 0, 31, 0.78)'
//     },
//     headerText: {
//         paddingTop: 5,
//         fontSize: 20,
//         color: '#BA77FF'
//     },
//     bigText: {
//         fontSize: 30,
//         fontWeight: '900',
//         paddingTop:20
//     }
// }

// <RNChart style={styles.chart}
//          chartData={chartData}
//          verticalGridStep={5}
//          xLabels={xLabels}
// />