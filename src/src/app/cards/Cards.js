/**
 * Created by ikedi on 02/06/2016.
 */
import React, {Component} from 'react';
import {
    Container,
    Header,
    Content,
    List,
    ListItem,
    Text,
    Icon,
    Title,
    Button,
    Card,
    CardItem,
    Thumbnail,
    InputGroup,
    Input,
    Picker,
    Switch
} from 'native-base';
import { Image, Dimensions } from 'react-native';
import { SegmentedControls } from 'react-native-radio-buttons';
import ScrollableTabView from 'react-native-scrollable-tab-view';

import HeaderButton from './../core/HeaderButton';
import ManualCard from './ManualCard';
import ScanCard from './ScanCard';
import ViewCards from './ViewCards';
//import Switch from 'react-native';

let ScreenWidth = Dimensions.get("window").width;
ScreenWidth = ScreenWidth * 0.83;

export default class Cards extends Component {
    constructor(props) {
        super(props);
    }

    render(){

        return (
            <Container style={styles.container}>
                <Header style={styles.header}>
                    <HeaderButton
                        openMenu={this.props.openMenu}
                        renderIcon={this.props.renderIcon}/>
                    <Image
                        style={styles.image}
                        source={require('../../../img/logo3.png')}/>
                </Header>
                <Content style={styles.content}>
                    <ScrollableTabView>
                        <ManualCard
                            renderIcon={this.props.renderIcon}
                            tabLabel="Manual Entry"
                            realmLocalStorage={this.props.realmLocalStorage}
                        />
                        
                        <ScanCard
                            renderIcon={this.props.renderIcon}
                            tabLabel="Scan Card"
                            realmLocalStorage={this.props.realmLocalStorage}
                        />

                        <ViewCards
                            renderIcon={this.props.renderIcon}
                            tabLabel="Registered Cards"
                            realmLocalStorage={this.props.realmLocalStorage}
                        />
                    </ScrollableTabView>
                </Content>
            </Container>
        )
    }
}


let styles = {
    container: {
        flex: 1,
        backgroundColor: 'transparent',
        flexDirection: 'column'
    },
    header: {
        flex: 2,
        backgroundColor: '#BB77FF'
    },
    content: {
        flex: 1,
        margin: 10,
        paddingBottom: 0,
        // borderWidth: 1,
        // borderColor: 'grey',
        backgroundColor: 'white'
    },
    footer: {
        flex: 1,
        backgroundColor: 'transparent'
    },
    image: {
        resizeMode:"contain",
        width: 40,
        height: 40
    },
    button: {
        backgroundColor: '#BB77FF',
        marginBottom: 0,
        borderRadius: 0
    },
    linkButton: {
        fontSize: 15,
        color: '#BB77FF',
        fontWeight: '900',
        borderColor: '#BB77FF',
        borderWidth: 1,
        borderRadius: 5,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 5,
        paddingLeft: 10
    },
    datePicker: {
        fontSize: 15,
        color: '#BB77FF',
        fontWeight: '900',
        // borderColor: '#BB77FF',
        borderColor: '#000',
        borderWidth: 1,
        borderRadius: 10,
        paddingTop: 3,
        paddingRight: 3,
        paddingBottom: 3,
        paddingLeft: 3,
        marginLeft: 3,
        marginRight: 10,
        width: ScreenWidth
    }
}

// let styles = {
//     background: {
//         backgroundColor: 'royalblue'
//     },
//     container: {
//         padding: 20
//     },
//     content: {
//         padding: 20
//     },
//     image: {
//         width: 335,
//         height: 300
//     },
//     button: {
//         backgroundColor: 'rgba(255, 0, 31, 0.78)'
//     },
//     headerText: {
//         paddingTop: 5,
//         fontSize: 20,
//         color: 'antiquewhite'
//     }
// }