/**
 * Created by ikedi on 02/06/2016.
 */
import React, {Component} from 'react';
import {
    Text,
    Icon,
    Button,
    Card,
    CardItem,
    InputGroup,
    Input
} from 'native-base';
import { Image, Dimensions, View } from 'react-native';
import { SegmentedControls } from 'react-native-radio-buttons';
import { CardIOView, CardIOUtilities } from 'react-native-card-io';
import DatePicker from 'react-native-datepicker';

let ScreenWidth = Dimensions.get("window").width;
ScreenWidth = ScreenWidth * 0.83;

export default class ScanCard extends Component {
    constructor(props) {
        super(props);

    }

    componentDidMount() {
        //make card.io launch faster
        CardIOUtilities.preload();
    }

    render(){
        return (
            <Card>
                <CardItem header>
                    <Text>Scan Card</Text>
                </CardItem>
                <CardItem>
                        <Text note>Hold up scan to camera to scan</Text>
                        <CardIOView
                            languageOrLocale="en_US"
                            guideColor="#FF0000"
                            useCardIOLogo={true}
                            hideCardIOLogo={false}
                            allowFreelyRotatingCardGuide={true}
                            scanInstructions="Hold card here. It will scan soon"
                            scanExpiry={true}
                            scannedImageDuration={2}
                            detectionMode={CardIOView.cardImageAndNumber}
                            didScanCard={result => console.log(result)}
                        />
                </CardItem>
                <CardItem>
                    <Text note>Card Details</Text>
                    <Text style={styles.linkButton}>Card #: Display Card Number Here</Text>
                    <Text style={styles.linkButton}>CVV: Display CVV Here (secured)</Text>
                    <Text style={styles.linkButton}>Expiry Date: Display Expiry Date Here</Text>
                    <Text note>Enter Card Pin</Text>
                    <InputGroup borderType="rounded" placeholder="Card PIN" secureTextEntry={true}>
                        <Icon name={this.props.renderIcon("key")} color="#900" size={30}/>
                        <Input type="number"/>
                    </InputGroup>
                </CardItem>
                <CardItem footer>
                    <Button rounded block style={styles.button}>
                        Register
                    </Button>
                </CardItem>
            </Card>

        )
    }
}

let styles = {
    container: {
        flex: 1,
        backgroundColor: 'transparent',
        flexDirection: 'column'
    },
    header: {
        flex: 2,
        backgroundColor: '#BB77FF'
    },
    content: {
        flex: 1,
        margin: 10,
        paddingBottom: 0,
        // borderWidth: 1,
        // borderColor: 'grey',
        backgroundColor: 'white'
    },
    footer: {
        flex: 1,
        backgroundColor: 'transparent'
    },
    image: {
        resizeMode:"contain",
        width: 40,
        height: 40
    },
    button: {
        backgroundColor: '#BB77FF',
        marginBottom: 0,
        borderRadius: 0
    },
    linkButton: {
        fontSize: 15,
        color: '#BB77FF',
        fontWeight: '900',
        borderColor: '#BB77FF',
        borderWidth: 1,
        borderRadius: 5,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 5,
        paddingLeft: 10
    }
}