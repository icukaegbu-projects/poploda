/**
 * Created by ikedi on 22/06/2016.
 */

import React, {Component} from 'react';
import {
    Text,
    Button,
    Card,
    CardItem,
    List,
    ListItem
} from 'native-base';
import { Image, Dimensions} from 'react-native';
//import { ListView } from 'realm/react-native';
import { SegmentedControls } from 'react-native-radio-buttons';
import Message from '../../api/Message';
import RegisteredCard from './RegisteredCard';

let ScreenWidth = Dimensions.get("window").width;
ScreenWidth = ScreenWidth * 0.83;

export default class ViewCards extends Component {
    constructor(props) {
        super(props);

        // let ds = new ListView.DataSource({rowHasChanged: (r1, r2) => r1 !== r2});
        //
        // this.state = {
        //     ds: ds,
        //     dataSource: []
        // }
        // this.state = {
        //     cards: []
        // }

        this._getCards = this._getCards.bind(this);
        this._loadMore = this._loadMore.bind(this);
        this._deleteCard = this._deleteCard.bind(this);
    }

    componentWillMount() {
        // this.setState({
        //     cards: this.props.realmLocalStorage.getCards()
        // });
        // let cards = this.props.realmLocalStorage.getCards();
        // this.setState({
        //     dataSource: this.state.ds.cloneWithRows(cards)
        // });
    }

    _getCards() {
        let cards = this.props.realmLocalStorage.getCards();
        console.log(cards);
        return cards.map((card) => {
            return <RegisteredCard
                cardType={card.cardType}
                card={card}
                renderIcon={this.props.renderIcon}
                deleteCard={this._deleteCard} />
        });
        // if(typeof cards !== 'undefined' && cards.length > 0){
        //
        // }else{
        //     return [<ListItem>No data</ListItem>];
        // }

        //save card details to localStorage
        // if(this.state.cards && this.state.cards.length > 1){
        //     return this.state.cards.map((card) => {
        //         return <RegisteredCard
        //             cardType={card.cardType}
        //             card={card}
        //             renderIcon={this.props.renderIcon}
        //             deleteCard={this._deleteCard} />
        //     });
        // }else{
        //     return [<ListItem>No data</ListItem>];
        // }

    }

    _loadMore () {

    }
    
    _deleteCard(cardNumber){
        this.props.realmLocalStorage.deleteCard(cardNumber);
    }
    
    render(){
        return (
            <Card>
                <CardItem header>
                    <Text>Registered Cards</Text>
                </CardItem>
                <CardItem>
                    <Text note>New Card Details</Text>
                    <List>
                        {
                            console.log('about to print')

                        }
                        {
                            this._getCards()
                        }
                    </List>
                </CardItem>
                <CardItem footer>
                    <Button
                        rounded block
                        style={styles.button}
                        ref="submit"
                        onPress={this._loadMore}>
                        Load More
                    </Button>
                </CardItem>
            </Card>
        )
    }
}

let styles = {
    container: {
        flex: 1,
        backgroundColor: 'transparent',
        flexDirection: 'column'
    },
    header: {
        flex: 2,
        backgroundColor: '#BB77FF'
    },
    content: {
        flex: 1,
        margin: 10,
        paddingBottom: 0,
        // borderWidth: 1,
        // borderColor: 'grey',
        backgroundColor: 'white'
    },
    footer: {
        flex: 1,
        backgroundColor: 'transparent'
    },
    image: {
        resizeMode:"contain",
        width: 40,
        height: 40
    },
    button: {
        backgroundColor: '#BB77FF',
        marginBottom: 0,
        borderRadius: 0
    },
    linkButton: {
        fontSize: 15,
        color: '#BB77FF',
        fontWeight: '900',
        borderColor: '#BB77FF',
        borderWidth: 1,
        borderRadius: 5,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 5,
        paddingLeft: 10
    },
    datePicker: {
        fontSize: 15,
        color: '#BB77FF',
        fontWeight: '900',
        // borderColor: '#BB77FF',
        borderColor: '#000',
        borderWidth: 1,
        borderRadius: 10,
        paddingTop: 3,
        paddingRight: 3,
        paddingBottom: 3,
        paddingLeft: 3,
        marginLeft: 3,
        marginRight: 10,
        width: ScreenWidth
    }
}


// <ListView
//     dataSource={this.state.dataSource}
//     renderRow={(card) => {
//                             return <RegisteredCard
//                                         cardType={card.cardType}
//                                         card={card}
//                                         renderIcon={this.props.renderIcon}
//                                         deleteCard={this._deleteCard} />
//                         }}/>