/**
 * Created by ikedi on 02/06/2016.
 */
import React, {Component} from 'react';
import {
    Text,
    Icon,
    Button,
    Card,
    CardItem,
    InputGroup,
    Input
} from 'native-base';
import { Image, Dimensions } from 'react-native';
import { SegmentedControls } from 'react-native-radio-buttons';
import DatePicker from 'react-native-datepicker';
import Message from '../../api/Message';

let ScreenWidth = Dimensions.get("window").width;
ScreenWidth = ScreenWidth * 0.83;

export default class ManualCard extends Component {
    constructor(props) {
        super(props);

        //retrieve current month date
        var currDate = new Date();

        this.state = {
            cardNumber: '',
            cvv: '',
            pin: '',
            cardType: 'None',
            selectedDate: currDate.getFullYear()+"-"+currDate.getMonth()
        }

        this._saveCard = this._saveCard.bind(this);
        this._setCardType = this._setCardType.bind(this);
        this._focusNextField = this._focusNextField.bind(this);
        this._clearState = this._clearState.bind(this);
        this._verifyCardDetails = this._verifyCardDetails.bind(this);
        //this._doesCardExist = this._doesCardExist.bind(this);
    }

    _setCardType(cardType){
        this.setState({
            cardType
        });
    }

    _saveCard() {
        //verify Card detail
        if(this._verifyCardDetails()){
            // let card = {
            //     cardNumber: parseInt(this.state.cardNumber),
            //     cardType: this.state.cardType,
            //     cvv: parseInt(this.state.cvv),
            //     expiryDate: new Date(this.state.selectedDate),
            //     pin: parseInt(this.state.pin)
            // };
            //Message.displayMessage(JSON.stringify(card));
            let card = {
                cardNumber: this.state.cardNumber,
                cardType: this.state.cardType,
                cvv: this.state.cvv,
                expiryDate: new Date(this.state.selectedDate),
                pin: this.state.pin
            };
            //save card details to localStorage
            this.props.realmLocalStorage.saveCard(card);

            this._clearState();
        }


    }

    _focusNextField(nextField) {
        this.refs[nextField].focus()
    }

    _clearState(){
        //retrieve current month date
        var currDate = new Date();

        this.setState({
            cardNumber: '',
            cvv: '',
            pin: '',
            cardType: 'None',
            selectedDate: currDate.getFullYear()+"-"+currDate.getMonth()
        })
    }

    _verifyCardDetails() {
        switch(this.state.cardType) {
            case 'None':
                Message.displayMessage('Card Type cannot be none. Select an option', 'TOP');
                return false;
                break;
            case 'MasterCard':
                if(this.state.cardNumber.length !== 16) {
                    Message.displayMessage('Card number must be 16 digits', 'TOP');
                    return false;
                }
            case 'Verve':
                if(this.state.cardNumber.length !== 16) {
                    Message.displayMessage('Card number must be 16 digits', 'TOP');
                    //this._focusNextField('cardNumber');
                    //this.refs.cardNumber.focus();
                    return false;
                }
                break;
            case 'Visa':
                if(this.state.cardNumber.length !== 13 && this.state.cardNumber.length !== 16) {
                    Message.displayMessage('Card number must be 13 or 16 digits', 'TOP');
                    return false;
                }
                break;
            default:
                //this._focusNextField('cvv');
        }
        // PIN = 4 digits; cvv = 3 digits
        if (this.state.cvv.length !== 3){
            Message.displayMessage('Card CVV must be 3 digits', 'TOP');
            this.setState({cvv: ''});
            return false;
        }

        if (this.state.pin.length !== 4){
            Message.displayMessage('Card PIN must be 4 digits', 'TOP');
            this.setState({pin: ''});
            return false;
        }

        //if all validations succeeded, return true
        return true;
    }

    // _doesCardExist() {
    //     let result = this.props.realmLocalStorage.doesCardExist(this.state.cardNumber);
    //
    //     if(result){
    //         Message.displayMessage('We exist', 'BOTTOM')
    //         return true;
    //     }
    //     return false;
    // }
    
    render(){
        const options = [
            "MasterCard",
            "Visa",
            "Verve"
        ];
        return (

            <Card>
                <CardItem header>
                    <Text>Register Card</Text>
                </CardItem>
                <CardItem>
                    <Text note>Choose Card Type</Text>
                    <SegmentedControls
                        options={ options }
                        onSelection={ this._setCardType }
                        selectedOption={ this.state.cardType }
                    />
                </CardItem>

                <CardItem>
                    <Text note>New Card Details</Text>

                    <InputGroup borderType="rounded" placeholder="Card Number">
                        <Icon name={this.props.renderIcon("card")} size={30} color="#900" />
                        <Input
                            ref="cardNumber"
                            value={this.state.cardNumber}
                            onChangeText={(cardNumber) => this.setState({cardNumber})}
                            autoFocus={true}
                            returnKeyType="next"
                            keyboardType="numeric"
                            maxLength={16}
                            onSubmitEditing={() => this._focusNextField('cvv')} />
                    </InputGroup>
                    <InputGroup borderType="rounded" placeholder="CVV" secureTextEntry={true}>
                        <Icon name={this.props.renderIcon("lock")} color="#900" size={30}/>
                        <Input
                            ref="cvv"
                            value={this.state.cvv}
                            onChangeText={(cvv) => this.setState({cvv})}
                            returnKeyType="next"
                            keyboardType="numeric"
                            maxLength={3}
                            onSubmitEditing={() => this._focusNextField('pin')} />
                    </InputGroup>
                    <InputGroup borderType="rounded" placeholder="Card PIN" secureTextEntry={true}>
                        <Icon name={this.props.renderIcon("key")} color="#900" size={30}/>
                        <Input
                            ref="pin"
                            value={this.state.pin}
                            onChangeText={(pin) => this.setState({pin})}
                            returnKeyType="next"
                            keyboardType="numeric"
                            maxLength={4}
                            onSubmitEditing={() => this._focusNextField('submit')} />
                    </InputGroup>
                    <Text note>Expiry Date</Text>
                    <DatePicker
                        mode="date"
                        style={styles.datePicker}
                        date={this.state.selectedDate}
                        format="YYYY-MM"
                        minDate="2016-05-1"
                        maxDate="2022-12-31"
                        confirmBtnText="Confirm"
                        cancelBtnText="Cancel"
                        onDateChange={(selectedDate) => {this.setState({selectedDate})}}
                    />
                    <Text>
                        {
                            "cardNumber: " + this.state.cardNumber + "\ntype: " + typeof this.state.cardNumber
                        }
                    </Text>
                    <Text>
                        {
                            "cardType: " + this.state.cardType + "\ntype: " + typeof this.state.cardType
                        }
                    </Text>
                    <Text>
                        {
                            "cvv: " + this.state.cvv + "\ntype: " + typeof this.state.cvv
                        }
                    </Text>
                    <Text>
                        {
                            "pin: " + this.state.pin + "\ntype: " + typeof this.state.pin
                        }
                    </Text>
                    <Text>
                        {
                            "expiry: " + this.state.selectedDate + "\ntype: " + typeof this.state.selectedDate
                        }
                    </Text>
                </CardItem>
                <CardItem footer>
                    <Button
                        rounded block
                        style={styles.button}
                        ref="submit"
                        onPress={this._saveCard}>
                        Register
                    </Button>
                </CardItem>
            </Card>

        )
    }
}

let styles = {
    container: {
        flex: 1,
        backgroundColor: 'transparent',
        flexDirection: 'column'
    },
    header: {
        flex: 2,
        backgroundColor: '#BB77FF'
    },
    content: {
        flex: 1,
        margin: 10,
        paddingBottom: 0,
        // borderWidth: 1,
        // borderColor: 'grey',
        backgroundColor: 'white'
    },
    footer: {
        flex: 1,
        backgroundColor: 'transparent'
    },
    image: {
        resizeMode:"contain",
        width: 40,
        height: 40
    },
    button: {
        backgroundColor: '#BB77FF',
        marginBottom: 0,
        borderRadius: 0
    },
    linkButton: {
        fontSize: 15,
        color: '#BB77FF',
        fontWeight: '900',
        borderColor: '#BB77FF',
        borderWidth: 1,
        borderRadius: 5,
        paddingTop: 10,
        paddingRight: 10,
        paddingBottom: 5,
        paddingLeft: 10
    },
    datePicker: {
        fontSize: 15,
        color: '#BB77FF',
        fontWeight: '900',
        // borderColor: '#BB77FF',
        borderColor: '#000',
        borderWidth: 1,
        borderRadius: 10,
        paddingTop: 3,
        paddingRight: 3,
        paddingBottom: 3,
        paddingLeft: 3,
        marginLeft: 3,
        marginRight: 10,
        width: ScreenWidth
    }
}