/**
 * Created by ikedi on 22/06/2016.
 */

import React, {Component} from 'react';
import {
    ListItem,
    Thumbnail,
    Text,
    Button,
    Icon
} from 'native-base';
import { View } from 'react-native';

export default class RegisteredCard extends Component {
    constructor(props){
        super(props);

        this._buildThumbnail = this._buildThumbnail.bind(this);
        //this._formatDate = this._formatDate.bind(this);
    }

    _buildThumbnail() {
        if(this.props.cardType === 'MasterCard'){
            return <Thumbnail size={40} source={require('../../../img/mastercard.png')} />
        }else if(this.props.cardType === 'Visa') {
            return <Thumbnail size={40} source={require('../../../img/visa.png')} />
        }else if(this.props.cardType === 'Verve') {
            return <Thumbnail size={40} source={require('../../../img/verve.jpg')} />
        }else {
            return <Thumbnail size={40} source={require('../../../img/logo3.png')} />
        }
    }

    // _formatDate(d) {
    //     return `${d.getFullYear()}-${d.getMonth()}`
    // }

    render() {
        return (
            <ListItem>
                {this._buildThumbnail()}
                <View>
                    <Text style={styles.card}>{this.props.card.cardNumber}</Text>
                    <Text note>Date: {this.props.card.expiryDate} | CVV: {this.props.card.cvv}</Text>
                </View>
                <Button
                    small bordered danger
                    onPress={() => this.props.deleteCard(this.props.card.cardNumber)}>
                    <Icon name={this.props.renderIcon("trash")} size={30}/>
                </Button>
            </ListItem>
        )
    }
}

let styles = {
    card: {
        fontWeight: '700',
        fontSize: 20
    }
}