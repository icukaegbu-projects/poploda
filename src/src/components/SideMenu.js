/**
 * Created by ikedi on 20/05/2016.
 */
import React, {Component} from 'react';
import {
    Container,
    Content,
    List,
    ListItem,
    Button,
    Header,
    Text,
    Icon
} from 'native-base';

export default class SideMenu extends Component {
    constructor(props){
        super(props)
    }

    render(){
        return (
            <Container>
                <Header>
                    <Button transparent
                            onPress={this.props.closeMenu}>
                        <Icon name="close" />
                    </Button>
                    <Text style={styles.headerText}>Menu</Text>
                </Header>
                <Content style={styles}>
                    <List>
                        <ListItem>
                            <Button transparent>
                                <Icon name="person" />
                                My Profile
                            </Button>
                        </ListItem>
                        <ListItem>
                            <Button transparent>
                                <Icon name="briefcase" />
                                My Wallet
                            </Button>
                        </ListItem>
                        <ListItem>
                            <Button transparent>
                                <Icon name="cash" />
                                Buy Credit
                            </Button>
                        </ListItem>
                        <ListItem>
                            <Button transparent>
                                <Icon name="information" />
                                Transaction History
                            </Button>
                        </ListItem>
                        <ListItem>
                            <Button transparent>
                                <Icon name="card" />
                                Debit Cards
                            </Button>
                        </ListItem>
                        <ListItem>
                            <Button transparent>
                                <Icon name="log-out" />
                                Logout
                            </Button>
                        </ListItem>
                    </List>
                </Content>
            </Container>
        )
    }
}

let styles = {
    // background: {
    //     backgroundColor: 'royalblue',
    //     color: 'white'
    // },
    // container: {
    //     padding: 20
    // },
    // content: {
    //     padding: 20
    // },
    // image: {
    //     width: 335,
    //     height: 300
    // },
    // button: {
    //     backgroundColor: 'rgba(255, 0, 31, 0.78)'
    // },
    headerText: {
        paddingTop: 5,
        fontSize: 20,
        color: 'antiquewhite'
    },
    drawer: {
        shadowColor: '#000000',
        shadowOpacity: 0.8,
        shadowRadius: 3
    },
    main: {
        paddingLeft: 3
    }
}