/**
 * Created by ikedi on 14/05/2016.
 */
import React, {Component} from 'react-native';
//import {Grid, Col, Row} from 'react-native-easy-grid';
import {
    Container,
    Header,
    Content,
    Text,
    Icon,
    Title,
    Button,
    Card,
    CardItem,
    Thumbnail,
    InputGroup,
    Input
} from 'native-base';
import {Image} from 'react-native';
//import PoploadaHeader from './PoploadaHeader';

export default class Login extends Component{
    render(){
        return (

                <Container style={styles.container}>
                    <Header>
                        <Image
                            resizeMode="contain"
                            style={styles.image}
                            source={require('../../img/logo2.png')}/>
                    </Header>
                    <Content>
                        <InputGroup borderType="rounded" placeholder="Username/Email">
                            <Icon name="person" size={30} color="#900" />
                            <Input/>
                        </InputGroup>
                        <InputGroup borderType="rounded" placeholder="Password" secureTextEntry={true}>
                            <Icon name="locked" color="#900" size={30}/>
                            <Input/>
                        </InputGroup>
                        <Button rounded block style={styles.button}>
                            Sign In
                        </Button>
                    </Content>
                </Container>



        );
    }
}

let styles = {
    background: {
        backgroundColor: 'royalblue'
    },
    container: {
        // padding: 20
        flex: 1,
    },
    content: {
        padding: 20
    },
    image: {
        width: 300,
        height: 250,
        padding: 20
    },
    button: {
        backgroundColor: 'rgba(255, 0, 31, 0.78)'
    },
    headerText: {
        paddingTop: 5,
        fontSize: 20,
        color: 'antiquewhite'
    }
}
// <Container style={styles.container}>
//     <Header>
//         <Text style={styles.headerText}>Poploda</Text>
//     </Header>
//     <Content style={[styles.background]}>
//         <Card>
//             <CardItem header>
//                 <Image
//                     resizeMode="contain"
//                     style={styles.image}
//                     source={require('../../img/logo2.png')}/>
//             </CardItem>
//             <CardItem cardBody>
//                 <InputGroup borderType="rounded" placeholder="Username/Email">
//                     <Icon name="person" size={30} color="#900" />
//                     <Input/>
//                 </InputGroup>
//                 <InputGroup borderType="rounded" placeholder="Password" secureTextEntry={true}>
//                     <Icon name="locked" color="#900" size={30}/>
//                     <Input/>
//                 </InputGroup>
//             </CardItem>
//             <CardItem>
//                 <Button large rounded block style={styles.button}>
//                     Sign In
//                 </Button>
//             </CardItem>
//         </Card>
//     </Content>
// </Container>





// /**
//  * Created by ikedi on 14/05/2016.
//  */
// import React, {Component} from 'react-native';
// //import {Grid, Col, Row} from 'react-native-easy-grid';
// import {
//     Container,
//     Header,
//     Content,
//     Text,
//     Icon,
//     Title,
//     Button,
//     Card,
//     CardItem,
//     Thumbnail,
//     InputGroup,
//     Input
// } from 'native-base';
// import {Image} from 'react-native';
// //import PoploadaHeader from './PoploadaHeader';
//
// export default class Login extends Component{
//     render(){
//         return (
//             <Container>
//                 <Header>
//                     <Text style={styles.headerText}>Poploda</Text>
//                 </Header>
//                 <Content style={[styles.container, styles.background]}>
//                     <Card>
//                         <CardItem header style={{padding: 0}}>
//                             <Image
//                                 resizeMode="cover"
//                                 style={styles.image}
//                                 source={require('../../img/logo2.png')}/>
//                         </CardItem>
//                         <CardItem cardBody>
//                             <InputGroup borderType="rounded" placeholder="Username/Email">
//                                 <Icon name="person" size={30} color="#900" />
//                                 <Input/>
//                             </InputGroup>
//                             <InputGroup borderType="rounded" placeholder="Password" secureTextEntry={true}>
//                                 <Icon name="locked" color="#900" size={30}/>
//                                 <Input/>
//                             </InputGroup>
//                         </CardItem>
//                         <CardItem>
//                             <Button large rounded block style={styles.button}>
//                                 Sign In
//                             </Button>
//                         </CardItem>
//                     </Card>
//                 </Content>
//             </Container>
//         );
//     }
// }
//
// let styles = {
//     background: {
//         backgroundColor: 'royalblue'
//     },
//     container: {
//         padding: 20
//     },
//     content: {
//         padding: 20
//     },
//     image: {
//         width: 335,
//         height: 300
//     },
//     button: {
//         backgroundColor: 'rgba(255, 0, 31, 0.78)'
//     },
//     headerText: {
//         paddingTop: 5,
//         fontSize: 20,
//         color: 'antiquewhite'
//     }
// }