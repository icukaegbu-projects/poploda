/**
 * Created by ikedi on 14/05/2016.
 */
import React, {Component} from 'react-native';
import {
    Container,
    Header,
    Content,
    List,
    ListItem,
    Text,
    Icon,
    Title,
    Button,
    Card,
    CardItem,
    Thumbnail,
    InputGroup,
    Input,
    Picker,
    Switch
} from 'native-base';
import { SegmentedControls } from 'react-native-radio-buttons'
//import Switch from 'react-native';

export default class Purchase extends Component {
    constructor(props) {
        super(props);

        this.state = {
            selectedOption: 1
        }
    }

    setSelectedOption(selectedOption){
        this.setState({
            selectedOption
        });
    }
    render(){
        const options = [
            "Airtel",
            "Etisalat",
            "Glo",
            "MTN"
        ];
        return (
            <Container>
                <Header>
                    <Text style={styles.headerText}>Poploda</Text>
                </Header>
                <Content style={[styles.container, styles.background]}>
                    <Card>
                        <CardItem header>
                            <Thumbnail source={require('../../img/logo2.png')}/>
                            <Text>Buy Airtime</Text>
                        </CardItem>
                        <CardItem>
                            <Text note>Choose Network</Text>
                            <SegmentedControls
                                options={ options }
                                onSelection={ this.setSelectedOption.bind(this) }
                                selectedOption={ this.state.selectedOption }

                            />
                        </CardItem>
                        <CardItem>
                            <Text note>Choose Amount</Text>
                            <InputGroup borderType="rounded" placeholder="Amount">
                                <Icon name="briefcase" color="#900" size={30}/>
                                <Input type="number"/>
                            </InputGroup>
                            <InputGroup borderType="rounded" placeholder="Phone">
                                <Icon name="android-phone-portrait" color="#900" size={30}/>
                                <Input type="tel"/>
                            </InputGroup>

                        </CardItem>

                        <CardItem>
                            <Text note>Make Payment</Text>
                            <Text>From Wallet</Text>
                            <Switch style={{marginBottom: 10}}/>

                            <Text>Registered Card</Text>
                            <Switch style={{marginBottom: 10}} value={true}/>

                            <Text>New Card Details</Text>
                            <InputGroup borderType="rounded" placeholder="Card Number">
                                <Icon name="card" size={30} color="#900" />
                                <Input type="number"/>
                            </InputGroup>
                            <InputGroup borderType="rounded" placeholder="CVV" secureTextEntry={true}>
                                <Icon name="locked" color="#900" size={30}/>
                                <Input type="number"/>
                            </InputGroup>
                            <InputGroup borderType="rounded" placeholder="Card PIN" secureTextEntry={true}>
                                <Icon name="pin" color="#900" size={30}/>
                                <Input type="number"/>
                            </InputGroup>
                        </CardItem>
                        <CardItem>
                            <Text note>Confirm Purchase</Text>
                            <InputGroup borderType="rounded" placeholder="Transaction PIN" secureTextEntry={true}>
                                <Icon name="pin" color="#900" size={30}/>
                                <Input type="number"/>
                            </InputGroup>
                        </CardItem>
                        <CardItem>
                            <Button large rounded block style={styles.button}>
                                Save
                            </Button>
                        </CardItem>
                    </Card>
                </Content>
            </Container>
        )
    }
}

let styles = {
    background: {
        backgroundColor: 'royalblue'
    },
    container: {
        padding: 20
    },
    content: {
        padding: 20
    },
    image: {
        width: 335,
        height: 300
    },
    button: {
        backgroundColor: 'rgba(255, 0, 31, 0.78)'
    },
    headerText: {
        paddingTop: 5,
        fontSize: 20,
        color: 'antiquewhite'
    }
}

// <Picker>
//     <Picker.Item label="Airtel" value="airtel" />
//     <Picker.Item label="Etisalat" value="etisalat" />
//     <Picker.Item label="Glo" value="glo" />
//     <Picker.Item label="MTN" value="mtn" />
// </Picker>
// <InputGroup borderType="rounded" placeholder="Network">
//     <Icon name="network" size={30} color="#900" />
//     <Input type="radio" label="Airtel"/>
//     <Input type="radio" label="Etisalat"/>
//     <Input type="radio" label="Glo"/>
//     <Input type="radio" label="MTN"/>
// </InputGroup>