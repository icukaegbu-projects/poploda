/**
 * Created by ikedi on 23/05/2016.
 */
import React, {Component} from 'react';
import {
    Image
} from 'react-native';
import {
    Button
} from 'native-base';

export default class Landing extends Component{
    render(){
        return (
            <Image source={require('../../img/backdrop.jpeg')} resizeMode="cover">
                <Image source={require('../../img/logo2.png')} />
            </Image>
        )
    }
}

let styles = {
    
}