/**
 * Created by ikedi on 15/05/2016.
 */
import React, {Component} from 'react';
import {
    Container,
    Header,
    Content,
    Card,
    CardItem,
    Text,
    Icon,
    Button,
    Thumbnail
} from 'native-base';
import RNChart from 'react-native-chart';

const chartData = [
    {
        name: 'BarChart',
        type: 'bar',
        color:'purple',
        widthPercent: 0.6,
        data: [30, 1, 1, 2, 3, 5, 21, 13, 21, 34, 55, 30],
    },
    {
        name: 'LineChart',
        color: 'gray',
        lineWidth: 2,
        highlightIndices: [1, 2],   // The data points at indexes 1 and 2 will be orange
        highlightColor: 'orange',
        showDataPoint: true,
        data: [10, 12, 14, 25, 31, 52, 41, 31, 52, 66, 22, 11],
    }
];

const xLabels = ['0','1','2','3','4','5','6','7','8','9','10','11'];

export default class Home extends Component {
    render(){
        return (
            <Container>
                <Header style={styles.header}>
                    <Button transparent
                        onPress={this.props.openMenu}>
                        <Icon name="android-menu" style={{color: '#BA77FF'}} />
                    </Button>
                    <Text style={styles.headerText}>Poploda</Text>
                </Header>
                <Content style={[styles.container, styles.background]}>
                    <Card>
                        <CardItem header>
                            <Thumbnail source={require('../../img/logo2.png')}/>
                            <Text>Home</Text>
                        </CardItem>
                        <CardItem>
                            <Text note>Wallet</Text>
                            <Text>Current Balance:</Text>
                            <Text style={styles.bigText}>12,500</Text>
                            <Button bordered danger>Fund Wallet</Button>
                        </CardItem>
                        <CardItem>
                            <Text note>Total Purchase: Last 7 days</Text>
                            <RNChart style={styles.chart}
                                     chartData={chartData}
                                     verticalGridStep={5}
                                     xLabels={xLabels}
                            />
                        </CardItem>
                    </Card>

                </Content>    
            </Container>
        )
    }
}



let styles = {
    background: {
        // backgroundColor: 'royalblue'
        backgroundColor: '#BA77FF'
    },
    container: {
        padding: 20
    },
    header: {
        backgroundColor: '#FFFFFF'
    },
    content: {
        padding: 20
    },
    image: {
        width: 335,
        height: 300
    },
    button: {
        backgroundColor: 'rgba(255, 0, 31, 0.78)'
    },
    headerText: {
        paddingTop: 5,
        fontSize: 20,
        color: '#BA77FF'
    },
    bigText: {
        fontSize: 30,
        fontWeight: '900',
        paddingTop:20
    },
    chart: {
        // position: 'absolute',
        // top: 16,
        // left: 4,
        // bottom: 4,
        // right: 16,
        width: 300,
        height: 300
    }
}