/**
 * Created by ikedi on 14/05/2016.
 */
import React, {Component} from 'react-native';
import {
    Container,
    Header,
    Content,
    List,
    ListItem,
    Text,
    Icon,
    Title,
    Button,
    Card,
    CardItem,
    Thumbnail,
    InputGroup,
    Input
} from 'native-base';
import Image from 'react-native';

export default class Profile extends Component {
    render(){
        return (
            <Container>
                <Header>
                    <Text style={styles.headerText}>Poploda</Text>
                </Header>
                <Content style={[styles.container, styles.background]}>
                    <Card>
                        <CardItem header>
                            <Thumbnail source={require('../../img/logo2.png')}/>
                            <Text>Profile</Text>
                        </CardItem>
                        <CardItem>
                            <Text note>Personal</Text>
                            <InputGroup borderType="rounded" placeholder="Full Name">
                                <Icon name="person" size={30} color="#900" />
                                <Input/>
                            </InputGroup>
                            <InputGroup borderType="rounded" placeholder="Email">
                                <Icon name="email" color="#900" size={30}/>
                                <Input type="email"/>
                            </InputGroup>
                            <InputGroup borderType="rounded" placeholder="Phone">
                                <Icon name="android-phone-portrait" color="#900" size={30}/>
                                <Input type="tel"/>
                            </InputGroup>
                        </CardItem>
                        <CardItem>
                            <Text note>Security</Text>
                            <InputGroup borderType="rounded" placeholder="UserName">
                                <Icon name="person" size={30} color="#900" />
                                <Input/>
                            </InputGroup>
                            <InputGroup borderType="rounded" placeholder="Password" secureTextEntry={true}>
                                <Icon name="locked" color="#900" size={30}/>
                                <Input type="password"/>
                            </InputGroup>
                            <InputGroup borderType="rounded" placeholder="Confirm Password" secureTextEntry={true}>
                                <Icon name="locked" color="#900" size={30}/>
                                <Input type="password"/>
                            </InputGroup>
                            <InputGroup borderType="rounded" placeholder="Transaction PIN" secureTextEntry={true}>
                                <Icon name="pin" color="#900" size={30}/>
                                <Input type="password"/>
                            </InputGroup>
                        </CardItem>
                        <CardItem>
                            <Button large rounded block style={styles.button}>
                                Save
                            </Button>
                        </CardItem>
                    </Card>
                </Content>
            </Container>
        )
    }
}

let styles = {
    background: {
        backgroundColor: 'royalblue'
    },
    container: {
        padding: 20
    },
    content: {
        padding: 20
    },
    image: {
        width: 335,
        height: 300
    },
    button: {
        backgroundColor: 'rgba(255, 0, 31, 0.78)'
    },
    headerText: {
        paddingTop: 5,
        fontSize: 20,
        color: 'antiquewhite'
    }
}

// <InputGroup borderType="rounded" placeholder="Security Question">
//     <Icon name="settings" color="#900" size={30}/>
//     <Input/>
// </InputGroup>
// <InputGroup borderType="rounded" placeholder="Security Question Answer">
//     <Icon name="thumbsup" color="#900" size={30}/>
//     <Input/>
//     </InputGroup>