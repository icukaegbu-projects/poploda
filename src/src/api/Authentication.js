/**
 * Created by ikedi on 05/06/2016.
 */
import React from 'react';
import {Actions} from 'react-native-router-flux';
import Connection from './Connection';
import Message from './Message';
import RealmLocalStorage from './RealmLocalStore';

export default {
    APP_URL: '',
    //RealmLocalStorage: new RealmLocalStorage(),
    init(url){
        //super();ß
        //setup connections to Realm and Firebase
        this.APP_URL = url;
    },
    login(email, password, fullname){
        //this.displayMessage('Called Login: '+this.APP_URL);

        //check if there is connection (cellular or wifi)
            //if yes, authenticate user against Firebase
            //if successful, display message starting with "ONLINE" and route to home
            //if unsuccessful, display error
        //if no connection
            //try local authentication: authenticate against Realm.io database
            //if successful, display message starting with "OFFLINE" and route to home
            //if unsuccessful, display error
        //for now, connect to firebase and write the username and data to it
        //this.APP_URL.authWithPassword({
        Connection.APP.authWithPassword({
            email: email,
            password: password
        }, (error, authData) => {
            if(error){
                Message.displayMessage("Invalid Login! Try Again\n"+error);
                return false;
            }
            Message.displayMessage('Login Successful: '+authData.password.email);

            //UNCOMMENT OUT REALM
            // RealmLocalStorage.init();
            // RealmLocalStorage.saveProfile({
            //     uid: authData.uid,
            //     fullName: fullname,
            //     email: email,
            //     password: password,
            //     authToken: authData.token
            // });
            //
            // //retrieve profile
            // RealmLocalStorage.getProfile(authData.uid);

            Actions.home();
            
            return true;
        });

    },

    logout(){
        //clean up, then logout and route back to login page
        Connection.APP.unauth();
        Message.displayMessage('Logged Out')
    },

    signup(email, password, fullname){
        //sign user up on Firebase
        //save data to firebase and realm
        //if successful, log user in and
            // save a copy of user credentials (uid returned by firebase)
            // to local Realm.io
            //then log user in
        //if unsuccessful, display corresponding error message and allow user to retry

        Connection.APP.createUser({
            email: email,
            password: password
        }, (error, userData) => {
            if(error){
                Message.displayMessage(error);
            }

            this._saveUserProfile(userData.uid, email, password,fullname);

            //Message.displayMessage("Account created successfully. Logging in");
            this.login(email, password, fullname);
        });
    },

    passwordReset(){

    },

    /**PRIVATE METHODS**/
    _saveUserProfile(uid, email, password, fullname){
        //INSTEAD OF PASSWORD, SAVE USER AUTH TOKEN
        //USE IT TO LOG IN USER WHEN NECESSARY
        let userPath = 'users/'+uid;
        Connection.APP.child(userPath).set({email, password, fullname}, function(error){
            if(error){
                Message.displayMessage("Error: " + error);
            }else{
                Message.displayMessage("Account created successfully.");
            }
        });

        //save userdate to Realm.io

    }
}