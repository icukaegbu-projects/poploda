/**
 * Created by ikedi on 07/06/2016.
 */
import Toast from 'react-native-root-toast';

export default {
    displayMessage(message, location){
        Toast.show(message, {
            duration: Toast.durations.SHORT,
            position: location === 'BOTTOM' ? Toast.positions.BOTTOM : Toast.positions.TOP,
            shadow: true,
            animation: true,
            hideOnPress: true,
            delay: 0
        });
    }
}