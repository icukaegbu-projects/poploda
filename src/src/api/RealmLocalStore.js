/**
 * Created by ikedi on 09/06/2016.
 */
import Realm from 'realm';
import Message from './Message';

// const UserSchema = {
//     name: 'User',
//     properties: {
//         uid: 'string',
//         fullName: 'string',
//         email: 'string',
//         password: 'string',
//         authToken: 'string'
//     }
// }
//
// const TransactionSchema = {
//     name: 'Transactions',
//     properties: {
//         uid: 'string',
//         amount: 'int',
//         network: 'string',
//         phone: 'string',
//         date: {type: 'date', default: new Date()},
//         month: 'int',
//         paymentType: 'string'
//     }
// }
//
// let realm = new Realm({schema: [UserSchema, TransactionSchema]})

export default {
    init(){
        //set the default path for the Realm DB
        Realm.defaultPath = '';

        //initialize the Realm storage, create if it doesnt exist
        const UserSchema = {
            name: 'User',
            primaryKey: 'uid',
            properties: {
                uid: 'string',
                fullName: 'string',
                email: 'string',
                password: 'string',
                authToken: 'string'
            }
        }

        const TransactionSchema = {
            name: 'Transactions',
            properties: {
                uid: 'string',
                amount: 'int',
                network: 'string',
                phone: 'string',
                date: {type: 'date', default: new Date()},
                month: 'int',
                paymentType: 'string'
            }
        }

        const CardDetailsSchema = {
            name: 'CardDetails',
            primaryKey: 'cardNumber',
            properties: {
                cardType: 'string', //MasterCard, Visa, Verve
                cardNumber: 'string',
                cvv: 'string',
                expiryDate: {type: 'date', default: new Date()},
                pin: 'string'
            }
        }

        this.realm = new Realm({
            schemaVersion: 2,
            schema: [UserSchema, TransactionSchema, CardDetailsSchema]
        });

    },

    //persist login time
    persistLogin(){

    },

    saveCard(card){
        //Message.displayMessage('Starting to save Card', 'TOP');
        let cardDetails = {
            cardNumber: card.cardNumber,
            cardType: card.cardType,
            cvv: card.cvv,
            expiryDate: card.expiryDate,
            pin: card.pin
        };
        
        // if(this._doesCardExist(card.cardNumber)){
        //     return false;
        // }else{
        //     this.realm.write(() => {
        //         let cd = this.realm.create('CardDetails', cardDetails);
        //     });
        //
        //     Message.displayMessage('Card saved successful', 'BOTTOM');
        // }

        this.realm.write(() => {
            let cards = this.realm.objects('CardDetails')
            let card = cards.filtered('cardNumber==$0', cardDetails.cardNumber)[0];
            //Message.displayMessage(`Card ${card.cardNumber} Exists`, 'BOTTOM');
            //Message.displayMessage(JSON.stringify(card), 'BOTTOM');

            if(card && card.cardNumber === cardDetails.cardNumber){
                Message.displayMessage(`Card ${cardDetails.cardNumber} Exists`, 'BOTTOM')
                return;
            }else{
                let cd = this.realm.create('CardDetails', cardDetails);
                Message.displayMessage('Card saved successful', 'BOTTOM');
            }
        });

    },

    getCards(){
       let cards = []; //, cards0 = {};

        this.realm.write(() => {
            let cardDetails = this.realm.objects('CardDetails');

            cardDetails.forEach((card) => {
                let ed = `${card.expiryDate.getFullYear()}-${card.expiryDate.getMonth()}`
                let c = {
                    cardNumber: card.cardNumber,
                    cvv: card.cvv,
                    pin: card.pin,
                    expiryDate: ed,
                    cardType: card.cardType
                }
                cards.push(c);
            });

            //cards0 = cardDetails;
        });

        return cards;
        //return cards0;
    },

    deleteCard(cardNumber){
        this.realm.write(() => {
            let card = this.realm.objects('CardDetails').filtered('cardNumber=$0',cardNumber)[0];
            this.realm.delete(card);
        });
        Message.displayMessage(`${cardNumber} deleted`);
    },

    _doesCardExist(cardNumber) {
        this.realm.write(() => {
            let cards = this.realm.objects('CardDetails')
            let card = cards.filtered('cardNumber==$0', cardNumber);

            if(card){
                Message.displayMessage('Card Exists', 'BOTTOM')
                return true;
            }
            return false;
        });
    },

    //persist profile
    saveProfile(user){
        let profile = {
            uid: user.uid,
            fullName: user.fullName,
            email: user.email,
            password: user.password,
            authToken: user.token
        };

        this.realm.write(() => {
            let u = this.realm.create('User', profile);
        });

        Message.displayMessage('Save successful', 'BOTTOM');
    },

    //read profile
    getProfile(uid){
        let user = this.realm.objects('User').filtered('uid=$0',uid);
        //Message.displayMessage('Found User '+user.email, 'BOTTOM');
        return user;
    },

    //persist transaction
    saveTransactions(){

    },

    saveTransaction(){

    },

    //retrieve specific transaction
    getTransaction(){

    },

    //retrieve all transactions
    getTransactions(){

    },

    //retrieve transactions for a specific month
    getTransactionsByMonth(){

    },

    //retrieve transactions for a specific network
    getTransactionsByNetwork(){

    }
}