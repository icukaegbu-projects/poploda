/**
 * Created by ikedi on 21/06/2016.
 */


const BASE_API = 'https://api.airvendng.net/vtu/';
//system supplied
let REF = '';
let USERNAME = '';
let PASSWORD = '';
let TYPE = '1'; //[1]airtime / [2]data bundle

//user supplier
let MSISDN = '';
let NETWORK_ID = '';  //1 Airtel, 2 MTN, 3 Glo, 4 Etisalat, 5 Visafone, 6 Test
let AMOUNT = '';

let REQUEST_API=`${BASE_API}?msisdn=${MSISDN}&networkid=${NETWORK_ID}&amount=${AMOUNT}&type=${TYPE}&ref=${REF}&username=${USERNAME}&password=${PASSWORD}`

// AIRVEND Response Codes
// 0: SUCCESSFUL transaction
// 99999999: FAILED: MNO Temporarily unavailable (MNO side offline)
// 99999998: FAILED: Credit Exceeded
// 99999997: FAILED: Incorrect MNO for MSISDN (wrong network/Invalid msisdn)

export default {
    purchase(msisdn, networdId, amount, type){
        let purchaseDetails = {
            "msisdn": msisdn,
            "networkid": networdId,
            "amount": amount,
            "type": type,
            "ref": REF,
            "username": USERNAME,
            "password": PASSWORD
        };

        var formBody = [];
        for (var property in purchaseDetails) {
            var encodedKey = encodeURIComponent(property);
            var encodedValue = encodeURIComponent(purchaseDetails[property]);
            formBody.push(encodedKey + "=" + encodedValue);
        }
        formBody = formBody.join("&");

        return fetch(BASE_API, {
            method: 'post',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/x-www-form-urlencoded'
            },
            body: formBody
        });
    }
}